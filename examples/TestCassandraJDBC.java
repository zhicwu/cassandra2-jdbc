import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.DatabaseMetaData;

public class TestCassandraJDBC {

  public static void main(String[] args) throws SQLException, ClassNotFoundException {

        String pConn   = args[0];
        String pSchema = args[1];
        String pTable  = args[2];

        String pSchemaTable = pSchema + "." + pTable;

	Class.forName("org.bigsql.cassandra2.jdbc.CassandraDriver");
	Connection jdbcConn = 
		DriverManager.getConnection("jdbc:cassandra://" + pConn);

	DatabaseMetaData databaseMetaData = jdbcConn.getMetaData();
	String driverVersion  = databaseMetaData.getDriverVersion();
	print("# JDBC Driver Version: " + driverVersion);

	print("\nexample.oorder");
	ResultSet rs3 = databaseMetaData.getColumns (null, pSchema, pTable, null);
	while(rs3.next()) {
		int    OrdinalPosition = rs3.getInt("ORDINAL_POSITION");
		String ColumnName      = rs3.getString("COLUMN_NAME");
		int    DataType        = rs3.getInt("DATA_TYPE");
		String TypeName        = rs3.getString("TYPE_NAME");
		int    ColumnSize      = rs3.getInt("COLUMN_SIZE");
		int    DecimalDigits   = rs3.getInt("DECIMAL_DIGITS");
		String IsNullable      = rs3.getString("IS_NULLABLE"); // "NO or empty string
		String IsAutoIncrement = rs3.getString("IS_AUTOINCREMENT");

		String NullState = "";
		if (IsNullable.equals("NO")) NullState = "NOT NULL";

		print("  " + rpad(ColumnName, 25) + rpad(TypeName, 20) + " " + 
                      ColumnSize + " " + DecimalDigits + " " + NullState);
	}


	String query = "select count(*) from " + pSchemaTable;

	Statement statement = jdbcConn.createStatement();
	ResultSet rs = statement.executeQuery(query);

	while (rs.next()) {
		print("\nRows in " + pSchemaTable + " = " + rs.getObject(1) + "\n");
	}

	rs.close();
	statement.close();
	jdbcConn.close();
  }

  private static void print(String s) {
	System.out.println(s);
  }

  private static String rpad(String str, int size) {
	StringBuffer padded = new StringBuffer(str);
  	while (padded.length() < size) {
    		padded.append(" ");
  	}
  	return padded.toString();
  }
}

