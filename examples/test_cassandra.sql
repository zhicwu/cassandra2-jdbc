DROP KEYSPACE example;

CREATE KEYSPACE example WITH REPLICATION = { 'class':'SimpleStrategy', 'replication_factor':1 };

CREATE TABLE example.oorder (
  id             INT PRIMARY KEY,
  o_id           INT,
  o_w_id         INT,
  o_d_id         INT,
  o_c_id         INT,
  o_carrier_id   INT,
  o_ol_cnt       decimal,
  o_all_local    decimal
);

COPY example.oorder FROM 'test.csv';

SELECT * FROM example.oorder;
