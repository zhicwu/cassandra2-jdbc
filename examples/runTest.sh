
if [ "x$CASSANDRA_HOME" == "x" ]; then
  echo "CASSANDRA_HOME environment not properly configured"
  exit 1
fi

myHost=`hostname -s`

echo ""
echo "## Running 'test_cassandra.sql'..."
echo " "
$CASSANDRA_HOME/bin/cqlsh $myHost < test_cassandra.sql

csLib="$PWD/../lib"

jdbc=`ls ../cassandra-jdbc-*.jar`
sl4j=`ls $csLib/slf4j-log4j*.jar`
sl4ja=$csLib/slf4j-api.jar
log4j=`ls $csLib/log4j*.jar`
clientutil=$csLib/cassandra-clientutil.jar
thrift=$csLib/cassandra-thrift.jar
libthrift=$csLib/libthrift.jar
guava=$csLib/guava.jar

cp=".:$jdbc:$sl4j:$sl4ja:$log4j:$clientutil:$thrift:$libthrift:$guava"

echo " "
echo "running with cp = $cp"
echo " "

java -cp $cp TestCassandraJDBC "$myHost:9160/example" "example" "oorder"

