# README #

The Cassandra 2.x JDBC driver.  For a binary distribution visit 
[http://cassandrajdbc.org](http://cassandrajdbc.org).



# Features #

* Cassandra 2.0 & 2.1 compatibility
* Examples & Lib directories demonstrating use with C* 2.0 & 2.1
* Cluster topology discovery
* Round robin connection scheme
* Primary and Backup DC configuration through JDBC URL


# Typical Use #

    import java.sql.Connection;
    import java.sql.PreparedStatement;

    ...

    Class.forName("org.bigsql.cassandra2.jdbc.CassandraDriver");
    Connection con = DriverManager.getConnection("jdbc:cassandra://host1--host2--host3:9160/keyspace1?primarydc=DC1&backupdc=DC2&consistency=QUORUM");

    String query = "UPDATE Test SET a=?, b=? WHERE KEY=?";
    PreparedStatement statement = con.prepareStatement(query);

    statement.setLong(1, 100);
    statement.setLong(2, 1000);
    statement.setString(3, "key0");

    statement.executeUpdate();

    statement.close();
