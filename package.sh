v=`grep "<property name=\"base.version\"" build.xml | cut -d'"' -f4`

dir=cassandra-jdbc-$v

echo dir=$dir

rm -rf $dir
mkdir $dir

cp -pr target/lib $dir/.
cp -p lib/* $dir/lib/.
cp -pr examples $dir/.
cp target/cassandra*.jar $dir/.


zip -r $dir.zip $dir

mv $dir.zip $HOME/.
ls -l $HOME/$dir.zip

